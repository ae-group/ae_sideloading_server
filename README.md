<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# sideloading_server 0.3.13

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_sideloading_server/develop?logo=python)](
    https://gitlab.com/ae-group/ae_sideloading_server)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_sideloading_server/release0.3.12?logo=python)](
    https://gitlab.com/ae-group/ae_sideloading_server/-/tree/release0.3.12)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_sideloading_server)](
    https://pypi.org/project/ae-sideloading-server/#history)

>ae_sideloading_server module 0.3.13.

[![Coverage](https://ae-group.gitlab.io/ae_sideloading_server/coverage.svg)](
    https://ae-group.gitlab.io/ae_sideloading_server/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_sideloading_server/mypy.svg)](
    https://ae-group.gitlab.io/ae_sideloading_server/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_sideloading_server/pylint.svg)](
    https://ae-group.gitlab.io/ae_sideloading_server/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_sideloading_server)](
    https://gitlab.com/ae-group/ae_sideloading_server/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_sideloading_server)](
    https://gitlab.com/ae-group/ae_sideloading_server/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_sideloading_server)](
    https://gitlab.com/ae-group/ae_sideloading_server/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_sideloading_server)](
    https://pypi.org/project/ae-sideloading-server/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_sideloading_server)](
    https://gitlab.com/ae-group/ae_sideloading_server/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_sideloading_server)](
    https://libraries.io/pypi/ae-sideloading-server)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_sideloading_server)](
    https://pypi.org/project/ae-sideloading-server/#files)


## installation


execute the following command to install the
ae.sideloading_server module
in the currently active virtual environment:
 
```shell script
pip install ae-sideloading-server
```

if you want to contribute to this portion then first fork
[the ae_sideloading_server repository at GitLab](
https://gitlab.com/ae-group/ae_sideloading_server "ae.sideloading_server code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_sideloading_server):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_sideloading_server/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.sideloading_server.html
"ae_sideloading_server documentation").
